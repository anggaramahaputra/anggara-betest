package com.example.msanggarabetest.service;

import com.example.msanggarabetest.model.UserInfo;
import com.example.msanggarabetest.model.UserInfo;
import com.example.msanggarabetest.repository.UserInfoRepository;
import com.example.msanggarabetest.repository.UserInfoRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class UserInfoServiceTest {

    @Mock
    private UserInfoRepository repository;

    @InjectMocks
    private  UserInfoService service;

    @Test
    public void testCreateUserInfo() {

        long userId = 2L;
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test", "test", "test");

        // Mock the behavior of the repository to return the mock
        Mockito.when(repository.findUserInfoByAccountNumber(mockUserInfo.getAccountNumber())).thenReturn(null);
        Mockito.when(repository.findUserInfoByRegistrationNumber(mockUserInfo.getRegistrationNumber())).thenReturn(null);

        // Mock the behavior of the repository to return the mock
        Mockito.when(repository.insert(mockUserInfo)).thenReturn(mockUserInfo);

        // Act
        Map<String, Object> result = service.createUserInfo(mockUserInfo);

        // Assert
        System.out.println(result);
        System.out.println(mockUserInfo);
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }

    @Test
    public void testUpdateUserInfo() {

        long userId = 2L;
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test",  "test", "test");

        // Mock the behavior of the repository to return the mock
        Mockito.when(repository.findById(String.valueOf(userId))).thenReturn(Optional.of(mockUserInfo));

        // Mock the behavior of the repository to return the mock
//        Mockito.when(repository.save(mockUserInfo)).thenReturn(mockUserInfo);

        // Act
        Map<String, Object> result = service.updateUserInfo(String.valueOf(userId), mockUserInfo);

        // Assert
        System.out.println(result);
        System.out.println(mockUserInfo);
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }

    @Test
    public void testDeleteUserInfo() {

        long userId = 2L;
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test",  "test", "test");

        // Mock the behavior of the repository to return the mock
        Mockito.when(repository.findById(String.valueOf(userId))).thenReturn(Optional.of(mockUserInfo));

        // Act
        Map<String, Object> result = service.deleteUserInfo(String.valueOf(userId));

        // Assert
        System.out.println(result);
        System.out.println(mockUserInfo);
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }


    @Test
    public void testGetUserInfo() {

        long userId = 2L;
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test",  "test", "test");

        // Mock the behavior of the repository to return the mock
        Mockito.when(repository.findById(String.valueOf(mockUserInfo.getUserId()))).thenReturn(Optional.of(mockUserInfo));

        // Act
        Map<String, Object> result = service.getUserInfo(String.valueOf(mockUserInfo.getUserId()));

        // Assert
        System.out.println(result);
        System.out.println(mockUserInfo);
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }
    @Test
    public void testGetUserInfoByAccountNumber() {

        long userId = 2L;
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test",  "test", "test");

        // Mock the behavior of the repository to return the mock
        Mockito.when(repository.findUserInfoByAccountNumber(String.valueOf(mockUserInfo.getAccountNumber()))).thenReturn(mockUserInfo);

        // Act
        Map<String, Object> result = service.getUserInfoByAccountNumber(String.valueOf(mockUserInfo.getAccountNumber()));

        // Assert
        System.out.println(result);
        System.out.println(mockUserInfo);
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }

    @Test
    public void testGetUserInfoByRegistrationNumber() {

        long userId = 2L;
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test",  "test", "test");

        // Mock the behavior of the repository to return the mock
        Mockito.when(repository.findUserInfoByRegistrationNumber(String.valueOf(mockUserInfo.getRegistrationNumber()))).thenReturn(mockUserInfo);

        // Act
        Map<String, Object> result = service.getUserInfoByRegistrationNumber(String.valueOf(mockUserInfo.getRegistrationNumber()));

        // Assert
        System.out.println(result);
        System.out.println(mockUserInfo);
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }

    @Test
    public void testGetAllUserInfo() {

        long userId = 2L;
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test",  "test", "test");

        List<UserInfo> userInfoList = new ArrayList<>();

        userInfoList.add(mockUserInfo);

        // Mock the behavior of the repository to return the mock
        Mockito.when(repository.findAll()).thenReturn(userInfoList);

        // Act
        Map<String, Object> result = service.getAllUserInfo();

        // Assert
        System.out.println(result);
        System.out.println(mockUserInfo);
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }
}
