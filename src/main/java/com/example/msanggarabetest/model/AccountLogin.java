package com.example.msanggarabetest.model;
import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "accountLogin")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@CompoundIndex(name = "userId_idx", def = "{'userId' : 1}")
public class AccountLogin implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    private String accountId;
    private String userName;
    private String password;
    private Date lastLoginDateTime;
    private String userId;
}
