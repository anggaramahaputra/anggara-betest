package com.example.msanggarabetest.controller;

import com.example.msanggarabetest.model.AccountLogin;
import com.example.msanggarabetest.model.Auth;
import com.example.msanggarabetest.model.UserInfo;
import com.example.msanggarabetest.repository.AccountLoginRepository;
import com.example.msanggarabetest.repository.UserInfoRepository;
import com.example.msanggarabetest.service.AccountLoginService;
import com.example.msanggarabetest.service.JwtService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.validator.internal.metadata.raw.BeanConfiguration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class AccountLoginControllerTest {

    private  MockMvc mockMvc;

    @Mock
    private AccountLoginService service;

    @Mock
    private JwtService jwtService;

    @InjectMocks
    private  AccountLoginRestController controller;

    @Autowired
    private ObjectMapper jacksonObjectMapper;

    @MockBean
    AuthenticationManager authenticationManager;

    @BeforeEach
    public  void setup(){
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        authenticationManager = mock(AuthenticationManager.class);
    }


    @Test
    public void testGet() throws Exception {


        String url= "/api/account/{id}";
        long accountId = 1L;
        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), "test");

        Map<String,Object> data = new HashMap<>();
        data.put("data",mockAccountLogin);
        data.put("success",true);
        data.put("error",null);

        Mockito.lenient().when(service.getAccountLogin(String.valueOf(accountId))).thenReturn(data);
        MvcResult mvcResult = this.mockMvc.perform(get(url,String.valueOf(accountId))).andExpect(status().isOk()).andReturn();


        assertEquals(200, mvcResult.getResponse().getStatus());
        assertNotNull(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void testGetAll() throws Exception {

        String url= "/api/account";
        long accountId = 1L;
        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), "test");
        List<AccountLogin> accountLoginList = new ArrayList<>();

        accountLoginList.add(mockAccountLogin);

        Map<String,Object> data = new HashMap<>();
        data.put("data",accountLoginList);
        data.put("success",true);
        data.put("error",null);

        Mockito.lenient().when(service.getAllAccountLogin()).thenReturn(data);
        MvcResult mvcResult = this.mockMvc.perform(get(url)).andExpect(status().isOk()).andReturn();


        assertEquals(200, mvcResult.getResponse().getStatus());
        assertNotNull(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void testGetAllLastLogin() throws Exception {

        String url= "/api/account";
        long accountId = 1L;
        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), "test");
        List<AccountLogin> accountLoginList = new ArrayList<>();

        accountLoginList.add(mockAccountLogin);

        Map<String,Object> data = new HashMap<>();
        data.put("data",accountLoginList);
        data.put("success",true);
        data.put("error",null);

        Mockito.lenient().when(service.getAllAccountLoginWhoLastLogin()).thenReturn(data);
        MvcResult mvcResult = this.mockMvc.perform(get(url)).andExpect(status().isOk()).andReturn();


        assertEquals(200, mvcResult.getResponse().getStatus());
        assertNotNull(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void testCreate() throws Exception {

        String url= "/api/account/register";
        long accountId = 1L;
        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), "test");

        Map<String,Object> data = new HashMap<>();
        data.put("data",mockAccountLogin);
        data.put("success",true);
        data.put("error",null);

        Mockito.lenient().when(service.createAccountLogin(mockAccountLogin)).thenReturn(data);
        MvcResult mvcResult = this.mockMvc.perform(put(url).contentType(MediaType.APPLICATION_JSON).content(jacksonObjectMapper.writeValueAsString(mockAccountLogin))).andExpect(status().isOk()).andReturn();

        System.out.println(mvcResult.getResponse().getContentAsString());
        assertEquals(200, mvcResult.getResponse().getStatus());
        assertNotNull(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void testUpdate() throws Exception {

        String url= "/api/account/{id}";
        long accountId = 1L;
        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), "test");

        Map<String,Object> data = new HashMap<>();
        data.put("data",mockAccountLogin);
        data.put("success",true);
        data.put("error",null);

        Mockito.lenient().when(service.updateAccountLogin(String.valueOf(accountId),mockAccountLogin)).thenReturn(data);
        MvcResult mvcResult = this.mockMvc.perform(patch(url, String.valueOf(accountId)).contentType(MediaType.APPLICATION_JSON).content(jacksonObjectMapper.writeValueAsString(mockAccountLogin))).andExpect(status().isOk()).andReturn();

        System.out.println(mvcResult.getResponse().getContentAsString());
        assertEquals(200, mvcResult.getResponse().getStatus());
        assertNotNull(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void testDelete() throws Exception {

        String url= "/api/account/{id}";
        long accountId = 1L;
        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), "test");

        Map<String,Object> data = new HashMap<>();
        data.put("data",mockAccountLogin);
        data.put("success",true);
        data.put("error",null);

        Mockito.lenient().when(service.deleteAccountLogin(String.valueOf(accountId))).thenReturn(data);
        MvcResult mvcResult = this.mockMvc.perform(delete(url, String.valueOf(accountId)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

        System.out.println(mvcResult.getResponse().getContentAsString());
        assertEquals(200, mvcResult.getResponse().getStatus());
        assertNotNull(mvcResult.getResponse().getContentAsString());

    }

//    @Test
//    public void testAuthenticateAndGetToken() throws Exception {
//
//        String url= "/api/account/login";
//        long accountId = 1L;
//        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), "test");
//        Auth auth = new Auth("test","test");
//        Authentication authentication = new TestingAuthenticationToken(auth.getUsername(), auth.getPassword());
//        Map<String,Object> data = new HashMap<>();
//        data.put("data",mockAccountLogin);
//        data.put("success",true);
//        data.put("error",null);
//
//        Mockito.lenient().when(authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(auth.getUsername(), auth.getPassword()))).thenReturn(authentication);
////        Mockito.when(authentication.isAuthenticated()).thenReturn(true);
//        Mockito.lenient().when(jwtService.generateToken(mockAccountLogin.getUserName())).thenReturn("test");
//        Mockito.lenient().when(service.updateLastLoginByUsername(mockAccountLogin.getUserName())).thenReturn(true);
//        MvcResult mvcResult = this.mockMvc.perform(post(url).contentType(MediaType.APPLICATION_JSON).content(jacksonObjectMapper.writeValueAsString(auth))).andExpect(status().isOk()).andReturn();
//
//        System.out.println(mvcResult.getResponse().getContentAsString());
//        assertEquals(200, mvcResult.getResponse().getStatus());
//        assertNotNull(mvcResult.getResponse().getContentAsString());
//
//    }

}
