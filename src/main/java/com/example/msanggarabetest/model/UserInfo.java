package com.example.msanggarabetest.model;
import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "userInfo")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@CompoundIndex(name = "userId_idx", def = "{'userId' : 1}")
public class UserInfo implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    private String userId;
    private String fullName;
    private String accountNumber;
    private String emailAddress;
    private String registrationNumber;
}
