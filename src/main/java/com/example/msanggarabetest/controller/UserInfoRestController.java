package com.example.msanggarabetest.controller;

import com.example.msanggarabetest.model.UserInfo;
import com.example.msanggarabetest.repository.UserInfoRepository;
import com.example.msanggarabetest.service.UserInfoService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserInfoRestController {

    private  final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserInfoService service;


    @GetMapping
    @Cacheable(value = "userInfo")
    public Map<String , Object> getAll(@RequestParam Optional<String> accountNumber, @RequestParam Optional<String> registrationNumber) {
        log.info("returning list of all users");
        Map<String , Object> result = new HashMap<>();

        if (String.valueOf(accountNumber) != null){
            result = service.getUserInfoByAccountNumber(String.valueOf(accountNumber));
            return result;
        }

        if (String.valueOf(registrationNumber) != null){
            result = service.getUserInfoByRegistrationNumber(String.valueOf(registrationNumber));
            return result;
        }


        result =  service.getAllUserInfo();

        return result;
    }

    @GetMapping("/{id}")
    @Cacheable(value = "userInfo", key = "#id")
    public Map<String , Object> get(@PathVariable String id) {
        log.info("returning user for id " + id);
        Map<String , Object> result = new HashMap<>();

        result = service.getUserInfo(id);

        return result;
    }

    @PutMapping("/register")
    @CachePut(value = "userInfo", key = "#user.userId", condition = "#user.userId != null")
    public Map<String, Object> insert(@Valid @RequestBody UserInfo user) {
        log.info("saving user details: " + user.toString());
        Map<String , Object> result = new HashMap<>();

        result = service.createUserInfo(user);

        return result;
    }
    @PatchMapping("/{id}")
    @CachePut(value = "userInfo", key = "#user.userId", condition = "#user.userId != null")
    public Map<String , Object>  update(@PathVariable String id, @Valid @RequestBody UserInfo user) {
        log.info("saving user details: " + user.toString());
        Map<String , Object> result = new HashMap<>();

        result = service.updateUserInfo(id, user);

        return result;
    }

    @DeleteMapping("/{id}")
    @CacheEvict(value = "userInfo", key = "#id")
    public Map<String , Object> delete(@PathVariable String id) {
        log.info("delete user by id " + id);
        Map<String , Object> result = new HashMap<>();

        result = service.deleteUserInfo(id);

        return result;
    }
}
