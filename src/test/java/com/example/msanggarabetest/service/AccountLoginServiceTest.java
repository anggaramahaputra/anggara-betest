package com.example.msanggarabetest.service;

import com.example.msanggarabetest.model.AccountLogin;
import com.example.msanggarabetest.model.UserInfo;
import com.example.msanggarabetest.repository.AccountLoginRepository;
import com.example.msanggarabetest.repository.UserInfoRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class AccountLoginServiceTest {

    @Mock
    private AccountLoginRepository repository;

    @Mock
    private UserInfoRepository userRepository;


    @InjectMocks
    private  AccountLoginService service;

    @Test
    public void testCreateAccountLogin() {

        long accountId = 1L;
        long userId = 2L;
        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), String.valueOf(userId));
        AccountLogin mockEmptyAccountLogin = new AccountLogin();
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test", "test", "test");

        // Mock the behavior of the repository to return the mock 
        Mockito.when(userRepository.findById(String.valueOf(userId))).thenReturn(Optional.of(mockUserInfo));
        Mockito.when(repository.findAccountLoginByUserId(String.valueOf(userId))).thenReturn(Optional.of(mockEmptyAccountLogin));
        Mockito.when(repository.insert(mockAccountLogin)).thenReturn(mockAccountLogin);

        // Act
        Map<String, Object> result = service.createAccountLogin(mockAccountLogin);

        // Assert
        
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }

    @Test
    public void testUpdateAccountLogin() {

        long accountId = 1L;
        long userId = 2L;
        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), String.valueOf(userId));

        // Mock the behavior of the repository to return the mock 
        Mockito.when(repository.findById(String.valueOf(accountId))).thenReturn(Optional.of(mockAccountLogin));

        // Mock the behavior of the repository to return the mock 
        Mockito.when(repository.save(mockAccountLogin)).thenReturn(mockAccountLogin);

        // Act
        Map<String, Object> result = service.updateAccountLogin(String.valueOf(accountId), mockAccountLogin);

        // Assert
        
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }

    @Test
    public void testDeleteAccountLogin() {

        long accountId = 1L;
        long userId = 2L;
        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), String.valueOf(userId));

        // Mock the behavior of the repository to return the mock 
        Mockito.when(repository.findById(String.valueOf(accountId))).thenReturn(Optional.of(mockAccountLogin));
        
        // Act
        Map<String, Object> result = service.deleteAccountLogin(String.valueOf(accountId));

        // Assert
        
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }


    @Test
    public void testGetAccountLogin() {

        long accountId = 1L;
        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), "test");

        // Mock the behavior of the repository to return the mock 
        Mockito.when(repository.findById(String.valueOf(mockAccountLogin.getAccountId()))).thenReturn(Optional.of(mockAccountLogin));

        // Act
        Map<String, Object> result = service.getAccountLogin(String.valueOf(mockAccountLogin.getAccountId()));

        // Assert
        
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }

    @Test
    public void testGetAllAccountLogin() {

        long accountId = 1L;
        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), "test");

        List<AccountLogin> accountLoginList = new ArrayList<>();

        accountLoginList.add(mockAccountLogin);

        // Mock the behavior of the repository to return the mock 
        Mockito.when(repository.findAll()).thenReturn(accountLoginList);

        // Act
        Map<String, Object> result = service.getAllAccountLogin();

        // Assert
        
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }
    @Test
    public void testGetAllAccountLoginWhoLastLogin() {

        long accountId = 1L;
        long MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24;

        AccountLogin mockAccountLogin = new AccountLogin(String.valueOf(accountId), "John Doe", "test", new Date(), "test");

        List<AccountLogin> accountLoginList = new ArrayList<>();

        accountLoginList.add(mockAccountLogin);

        Date threeDaysAgoDate = new Date(new Date().getTime() - MILLIS_IN_A_DAY);

        Criteria criteria = Criteria.where("lastLoginDateTime").gt(threeDaysAgoDate);

        // Mock the behavior of the repository to return the mock
        Mockito.lenient().when(repository.findAllBy(criteria)).thenReturn(accountLoginList);

        // Act
        Map<String, Object> result = service.getAllAccountLogin();

        // Assert
        assertNotNull(result.get("data"));
        assertEquals(true, result.get("success"));
        assertNull(result.get("error"));
    }
}
