package com.example.msanggarabetest.controller;

import com.example.msanggarabetest.model.UserInfo;
import com.example.msanggarabetest.model.UserInfo;
import com.example.msanggarabetest.repository.UserInfoRepository;
import com.example.msanggarabetest.repository.UserInfoRepository;
import com.example.msanggarabetest.service.UserInfoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class UserInfoControllerTest {

    private  MockMvc mockMvc;

    @Mock
    private UserInfoService service;

    @InjectMocks
    private  UserInfoRestController controller;

    @Autowired
    private ObjectMapper jacksonObjectMapper;


    @BeforeEach
    public  void setup(){
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }
    @Test
    public void testGet() throws Exception {


        String url= "/api/user/{id}";
        long userId = 1L;
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test", "test", "test");

        Map<String,Object> data = new HashMap<>();
        data.put("data",mockUserInfo);
        data.put("success",true);
        data.put("error",null);

        Mockito.lenient().when(service.getUserInfo(String.valueOf(userId))).thenReturn(data);
        MvcResult mvcResult = this.mockMvc.perform(get(url,String.valueOf(userId))).andExpect(status().isOk()).andReturn();


        assertEquals(200, mvcResult.getResponse().getStatus());
        assertNotNull(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void testGetAll() throws Exception {

        String url= "/api/user";
        long userId = 1L;
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test", "test", "test");
        List<UserInfo> userLoginList = new ArrayList<>();

        userLoginList.add(mockUserInfo);

        Map<String,Object> dataList = new HashMap<>();
        dataList.put("data",userLoginList);
        dataList.put("success",true);
        dataList.put("error",null);

        Map<String,Object> data = new HashMap<>();
        data.put("data",mockUserInfo);
        data.put("success",true);
        data.put("error",null);

        Mockito.lenient().when(service.getUserInfoByAccountNumber(mockUserInfo.getAccountNumber())).thenReturn(data);

        Mockito.lenient().when(service.getUserInfoByAccountNumber(mockUserInfo.getRegistrationNumber())).thenReturn(data);

        Mockito.lenient().when(service.getAllUserInfo()).thenReturn(dataList);


        MvcResult mvcResult = this.mockMvc.perform(get(url)).andExpect(status().isOk()).andReturn();

        assertEquals(200, mvcResult.getResponse().getStatus());
        assertNotNull(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void testCreate() throws Exception {

        String url= "/api/user/register";
        long userId = 1L;
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test", "test", "test");

        Map<String,Object> data = new HashMap<>();
        data.put("data",mockUserInfo);
        data.put("success",true);
        data.put("error",null);

        Mockito.lenient().when(service.createUserInfo(mockUserInfo)).thenReturn(data);
        MvcResult mvcResult = this.mockMvc.perform(put(url).contentType(MediaType.APPLICATION_JSON).content(jacksonObjectMapper.writeValueAsString(mockUserInfo))).andExpect(status().isOk()).andReturn();

        System.out.println(mvcResult.getResponse().getContentAsString());
        assertEquals(200, mvcResult.getResponse().getStatus());
        assertNotNull(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void testUpdate() throws Exception {

        String url= "/api/user/{id}";
        long userId = 1L;
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test", "test", "test");

        Map<String,Object> data = new HashMap<>();
        data.put("data",mockUserInfo);
        data.put("success",true);
        data.put("error",null);

        Mockito.lenient().when(service.updateUserInfo(String.valueOf(userId),mockUserInfo)).thenReturn(data);
        MvcResult mvcResult = this.mockMvc.perform(patch(url, String.valueOf(userId)).contentType(MediaType.APPLICATION_JSON).content(jacksonObjectMapper.writeValueAsString(mockUserInfo))).andExpect(status().isOk()).andReturn();

        System.out.println(mvcResult.getResponse().getContentAsString());
        assertEquals(200, mvcResult.getResponse().getStatus());
        assertNotNull(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void testDelete() throws Exception {

        String url= "/api/user/{id}";
        long userId = 1L;
        UserInfo mockUserInfo = new UserInfo(String.valueOf(userId), "John Doe", "test", "test", "test");

        Map<String,Object> data = new HashMap<>();
        data.put("data",mockUserInfo);
        data.put("success",true);
        data.put("error",null);

        Mockito.lenient().when(service.deleteUserInfo(String.valueOf(userId))).thenReturn(data);
        MvcResult mvcResult = this.mockMvc.perform(delete(url, String.valueOf(userId)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

        System.out.println(mvcResult.getResponse().getContentAsString());
        assertEquals(200, mvcResult.getResponse().getStatus());
        assertNotNull(mvcResult.getResponse().getContentAsString());

    }

}
