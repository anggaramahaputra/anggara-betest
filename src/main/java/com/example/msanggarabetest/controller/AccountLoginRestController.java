package com.example.msanggarabetest.controller;

import com.example.msanggarabetest.model.AccountLogin;
import com.example.msanggarabetest.model.Auth;
import com.example.msanggarabetest.model.AccountLogin;
import com.example.msanggarabetest.service.AccountLoginService;
import com.example.msanggarabetest.service.JwtService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/api/account")
public class AccountLoginRestController {
    @Autowired
    private  AccountLoginService service;
    @Autowired
    private  JwtService jwtService;
    @Autowired
    private  AuthenticationManager authenticationManager;

    private  final Logger log = LoggerFactory.getLogger(this.getClass());


    @GetMapping
    @Cacheable(value = "accountLogin")
    public Map<String , Object> getAll() {
        log.info("returning list of all account");
        Map<String , Object> result = new HashMap<>();
        result =  service.getAllAccountLogin();

        return result;
    }
    @GetMapping("/lastLogin")
    @Cacheable(value = "accountLogin")
    public Map<String , Object> getAllLastLogin() {
        log.info("returning list of all account");
        Map<String , Object> result = new HashMap<>();
        result =  service.getAllAccountLoginWhoLastLogin();

        return result;
    }

    @GetMapping("/{id}")
    @Cacheable(value = "accountLogin", key = "#id")
    public Map<String , Object> get(@PathVariable String id) {
        log.info("returning account for id " + id);
        Map<String , Object> result = new HashMap<>();

        result = service.getAccountLogin(id);

        return result;
    }

    @PutMapping("/register")
    @CachePut(value = "accountLogin", key = "#account.accountId", condition = "#account.accountId != null")
    public Map<String, Object> insert(@Valid @RequestBody AccountLogin account) {
        log.info("saving account details: " + account.toString());
        Map<String , Object> result = new HashMap<>();

        result = service.createAccountLogin(account);

        return result;
    }
    @PatchMapping("/{id}")
    @CachePut(value = "accountLogin", key = "#account.accountId", condition = "#account.accountId != null")
    public Map<String , Object>  update(@PathVariable String id, @Valid @RequestBody AccountLogin account) {
        log.info("saving account details: " + account.toString());
        Map<String , Object> result = new HashMap<>();

        result = service.updateAccountLogin(id, account);

        return result;
    }

    @DeleteMapping("/{id}")
    @CacheEvict(value = "accountLogin", key = "#id")
    public Map<String , Object> delete(@PathVariable String id) {
        log.info("delete account by id " + id);
        Map<String , Object> result = new HashMap<>();

        result = service.deleteAccountLogin(id);

        return result;
    }


    @PostMapping("/login")
    public  Map<String , Object> authenticateAndGetToken(@RequestBody Auth authRequest) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        AccountLogin accountLogin;
        Boolean success = false;
        Map<String , Object> result = new HashMap<>();
        Map<String , Object> data = new HashMap<>();

        if (authentication.isAuthenticated()) {
            String token = jwtService.generateToken(authRequest.getUsername());

            data.put("token", token);
            result.put("data", data);
            result.put("success", true);
            result.put("error", null);

            success =  service.updateLastLoginByUsername(authRequest.getUsername());

            if (!success){
                result.put("data", null);
                result.put("success", false);
                result.put("error", "cannot update last login");
            }

            return result;
        } else {
            result.put("data", null);
            result.put("success", false);
            result.put("error", null);
            return result;
        }
    }
}