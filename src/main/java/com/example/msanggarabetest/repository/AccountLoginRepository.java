package com.example.msanggarabetest.repository;

import com.example.msanggarabetest.model.AccountLogin;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface AccountLoginRepository extends MongoRepository<AccountLogin, String> {
    Optional<AccountLogin> findAccountLoginByUserName(String username);
    Optional<AccountLogin> findAccountLoginByUserId(String userId);
    List<AccountLogin> findAllBy(Criteria criteria) ;
}