package com.example.msanggarabetest.repository;

import com.example.msanggarabetest.model.UserInfo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserInfoRepository extends MongoRepository<UserInfo, String> {

     UserInfo findUserInfoByAccountNumber(String accountNumber);
     UserInfo findUserInfoByRegistrationNumber(String registrationNumber);

}