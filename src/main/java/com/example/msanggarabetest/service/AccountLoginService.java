package com.example.msanggarabetest.service;
import com.example.msanggarabetest.model.AccountLogin;
import com.example.msanggarabetest.model.UserInfo;
import com.example.msanggarabetest.repository.AccountLoginRepository;
import com.example.msanggarabetest.repository.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AccountLoginService implements UserDetailsService {
    @Autowired
    private  AccountLoginRepository repository;
    @Autowired
    private  UserInfoRepository userRepository;

    PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<AccountLogin> userDetail = repository.findAccountLoginByUserName(username);
        // Converting userDetail to UserDetails
        return userDetail.map(UserInfoDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("User not found " + username));
    }
    public Map<String, Object> createAccountLogin(AccountLogin accountLogin) {
        accountLogin.setPassword(encoder.encode(accountLogin.getPassword()).replace("{bcrypt}", ""));

        UserInfo tempUserInfo;
        AccountLogin tempAccountLogin;
        Map<String, Object> data = new HashMap<>();

        tempUserInfo = userRepository.findById(accountLogin.getUserId()).get();

        if (tempUserInfo.getUserId() == null) {
            data.put("data", null);
            data.put("error", "user not exist");
            data.put("success", false);
            return data;
        }

        tempAccountLogin = repository.findAccountLoginByUserId(accountLogin.getUserId()).get();

        if (tempAccountLogin.getUserId() != null) {
            data.put("data", null);
            data.put("error", "account already exist");
            data.put("success", false);
            return data;
        }

        try {
            repository.insert(accountLogin);
            data.put("data", accountLogin);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }

    public Map<String, Object> updateAccountLogin(String id , AccountLogin accountLogin) {
        AccountLogin tempAccountLogin;
        Map<String, Object> data = new HashMap<>();
        //get user by id
        tempAccountLogin = repository.findById(id).get();

        if (tempAccountLogin.getUserId() == null) {
            data.put("data", null);
            data.put("error", "user not exist");
            data.put("success", false);
            return data;
        }

        try {
            accountLogin.setPassword(encoder.encode(accountLogin.getPassword()).replace("{bcrypt}", ""));
            repository.save(accountLogin);
            data.put("data", accountLogin);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }


    public Map<String, Object> getAccountLogin(String id) {
        AccountLogin accountLogin;
        Map<String, Object> data = new HashMap<>();
        //get user by id
        try {
            accountLogin = repository.findById(id).get();
            data.put("data", accountLogin);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }

    public Boolean updateLastLoginByUsername(String username) {
        AccountLogin accountLogin;
        accountLogin = repository.findAccountLoginByUserName(username).get();
        Boolean success = false;
        if (accountLogin.getUserId() == null) {
            return false;
        }
        accountLogin.setLastLoginDateTime(new Date());
        try {
            repository.save(accountLogin);
            success = true;
        }catch (Exception e){
            System.out.println(e.toString());
            success = false;
        }

        return success;
    }

    public Map<String, Object> getAllAccountLogin() {
        List<AccountLogin> accountLogin;
        Map<String, Object> data = new HashMap<>();
        //get user by id
        try {
            accountLogin = repository.findAll();
            data.put("data", accountLogin);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }
    public Map<String, Object> getAllAccountLoginWhoLastLogin() {
        List<AccountLogin> accountLogin;
        Map<String, Object> data = new HashMap<>();
        long MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24;
        //get user by id
        try {
            Date threeDaysAgoDate = new Date(new Date().getTime() - MILLIS_IN_A_DAY);
            Criteria criteria = Criteria.where("lastLoginDateTime").gt(threeDaysAgoDate);

            accountLogin = repository.findAllBy(criteria);
            data.put("data", accountLogin);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }
    public Map<String, Object> deleteAccountLogin(String id) {
        AccountLogin accountLogin;
        Map<String, Object> data = new HashMap<>();
        //get user by id
        accountLogin = repository.findById(id).get();

        if (accountLogin.getUserId() == null) {
            data.put("data", null);
            data.put("error", "user not exist");
            data.put("success", false);
            return data;
        }

        try {
            repository.delete(accountLogin);
            data.put("data", accountLogin);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }



}