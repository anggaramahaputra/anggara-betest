package com.example.msanggarabetest.service;

import com.example.msanggarabetest.model.UserInfo;
import com.example.msanggarabetest.repository.AccountLoginRepository;
import com.example.msanggarabetest.repository.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserInfoService {
    @Autowired
    private  UserInfoRepository repository;

    public Map<String, Object> createUserInfo(UserInfo userInfo) {
        UserInfo tempUserInfo;
        Map<String, Object> data = new HashMap<>();
        //get user by account number & check it
        tempUserInfo = repository.findUserInfoByAccountNumber(userInfo.getAccountNumber());

        if (tempUserInfo != null) {
            data.put("data", null);
            data.put("error", "account number already exist");
            data.put("success", false);
            return data;
        }
        //get user by registration number & check it
        tempUserInfo = repository.findUserInfoByRegistrationNumber(userInfo.getRegistrationNumber());

        if (tempUserInfo != null) {
            data.put("data", null);
            data.put("error", "account number already exist");
            data.put("success", false);
            return data;
        }

        try {
            repository.insert(userInfo);
            data.put("data", userInfo);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }

    public Map<String, Object> updateUserInfo(String id, UserInfo userInfo) {
        UserInfo tempUserInfo;
        Map<String, Object> data = new HashMap<>();
        //get user by id
        tempUserInfo = repository.findById(id).get();

        if (tempUserInfo.getUserId() == null) {
            data.put("data", null);
            data.put("error", "user not exist");
            data.put("success", false);
            return data;
        }

        try {
            repository.insert(userInfo);
            data.put("data", userInfo);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }

    public Map<String, Object> getUserInfo(String id) {
        UserInfo userInfo;
        Map<String, Object> data = new HashMap<>();
        //get user by id
        try {
            userInfo = repository.findById(id).get();
            data.put("data", userInfo);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }

    public Map<String, Object> getUserInfoByAccountNumber(String accountNumber) {
        UserInfo userInfo;
        Map<String, Object> data = new HashMap<>();
        //get user by id
        try {
            userInfo = repository.findUserInfoByAccountNumber(accountNumber);
            data.put("data", userInfo);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }

    public Map<String, Object> getUserInfoByRegistrationNumber(String registrationNumber) {
        UserInfo userInfo;
        Map<String, Object> data = new HashMap<>();
        //get user by id
        try {
            userInfo = repository.findUserInfoByRegistrationNumber(registrationNumber);
            data.put("data", userInfo);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }

    public Map<String, Object> getAllUserInfo() {
        List<UserInfo> userInfo;
        Map<String, Object> data = new HashMap<>();
        //get user by id
        try {
            userInfo = repository.findAll();
            data.put("data", userInfo);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }
    public Map<String, Object> deleteUserInfo(String id) {
        UserInfo userInfo;
        Map<String, Object> data = new HashMap<>();
        //get user by id
        userInfo = repository.findById(id).get();

        if (userInfo.getUserId() == null) {
            data.put("data", null);
            data.put("error", "user not exist");
            data.put("success", false);
            return data;
        }

        try {
            repository.delete(userInfo);
            data.put("data", userInfo);
            data.put("error", null);
            data.put("success", true);
        }catch (Exception e){
            data.put("data", null);
            data.put("error", e.toString());
            data.put("success", false);
        }
        return data;
    }


}
